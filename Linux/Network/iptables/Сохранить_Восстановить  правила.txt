﻿Сохранить

iptables-save > /etc/iptables/rules.v4

Debian 12
iptables-save > /etc/iptables.up.rules

или
apt install iptables-persistent
service netfilter-persistent save


CentOS
service iptables save


Восстановить правила:
iptables-restore < /etc/iptables/rules.v4


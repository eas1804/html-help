apt-get install tmux


Старт
# tmux //без параметров будет создана сессия 0
# tmux new -s session1 //новая сессия session1. Название отображается снизу-слева в квадратных скобках в статус строке. Далее идет перечисление окон. Текущее окно помечается звездочкой.


Список окон
Ctrl+b w // переключиться курсором вверх-вниз

Новое окно 
Ctrl+b c

Список сессий
# tmux ls


Подключиться к работающей сессии
# tmux attach //подключение к сессии, либо к единственной, либо последней созданной
# tmux attach -t session1 // подключение к сессии session1

Выбрать сессию
<C-b s>

Завершение сессии
# tmux kill-session -t session1

Разделение окон по горизонтали
Войдите в режим разделения окна, нажав сочетание клавиш Ctrl + b, а затем нажмите " (Shift + 2).
Теперь вы можете работать с каждой частью независимо, переключаясь между ними с помощью сочетания клавиш Ctrl + b, а затем нажимая стрелку влево или стрелку вправо, чтобы переключиться между панелями.

По вертикали
Войдите в режим разделения окна, нажав сочетание клавиш Ctrl + b, а затем нажмите % (Shift + 5).
========================

Автозапуск в bash
nano ~/.bashrc

if [[ -z $TMUX && -n $SSH_TTY ]]; then
    me=$(whoami)
    real_tmux=$(command -v tmux)

    if [ -z $real_tmux ]; then
        echo "No tmux installed."
    fi

    export TERM="xterm-256color"

    if $real_tmux has-session -t $me 2>/dev/null; then
        $real_tmux attach-session -t $me
    else
        if [[ -n $SSH_TTY ]]; then
            (tmux new-session -d -s $me && tmux attach-session -t $me)
        fi
    fi
fi


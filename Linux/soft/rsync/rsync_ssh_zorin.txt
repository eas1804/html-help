root@foto-lesnoy:~/sh# cat backup_mega_from_zorin.sh 
#!/bin/bash

date
# Папка, куда будем складывать архивы
LOCAL_DIR=/mnt/disk2_WD/backup/
# Имя сервера, который архивируе
SRV_NAME=Zorin
# Адрес сервера, который архивируем
SRV_IP=172.16.181.244
# Пользователь rsync на сервере, который архивируем
SRV_USER=exch_adm
# Ресурс на сервере для бэкапа
SRV_DIR=/home/exch_adm/MEGA/

echo "Start backup ${SRV_NAME}"
# Создаем папку для инкрементных бэкапов
ls -l ${LOCAL_DIR}${SRV_NAME}/increment/ 2>/dev/nul || mkdir -p ${LOCAL_DIR}${SRV_NAME}/increment/
ls -l ${LOCAL_DIR}${SRV_NAME}/current/   2>/dev/nul || mkdir -p ${LOCAL_DIR}${SRV_NAME}/current/

# Запускаем  бэкап с параметрами
/usr/bin/rsync -avzP  --delete  \
 '-e ssh -i /root/sh/zorin.key'  \
  $SRV_USER@$SRV_IP:$SRV_DIR ${LOCAL_DIR}${SRV_NAME}/current/  \
 --backup --backup-dir=${LOCAL_DIR}${SRV_NAME}/increment/`date +%Y-%m-%d`/




# Оставляем 10 самых свежих подкаталогоы

ls -td ${LOCAL_DIR}${SRV_NAME}/increment/*/ | head -n 10 | xargs -d '\n' rm -r

date
echo "Finish backup ${srv_name}"

exit 0


-delete — удаления файлов, которых нет в исходной директории
-z,  включение режим сжатия;
-a,  режим архивирования, включает ключи -rlptgoD;
-v,  выводит имена копируемых файлов;
-P или—progress  отображение прогресса при копировании;

